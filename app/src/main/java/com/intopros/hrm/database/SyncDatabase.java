package com.intopros.hrm.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class SyncDatabase {
    public String TAG = "DBSync";

    public static void clearDatabase(Context context) {
        DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(
                context.getApplicationContext(), "hrm-db", null);
        SQLiteDatabase db = devOpenHelper.getWritableDatabase();
        devOpenHelper.onUpgrade(db, 0, 0);
    }

}


