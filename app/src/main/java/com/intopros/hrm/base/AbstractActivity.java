package com.intopros.hrm.base;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.intopros.hrm.R;
import com.intopros.hrm.base.baseutils.sharedpreferences.SharedPreferencesHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public abstract class AbstractActivity extends AppCompatActivity {
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @BindView(R.id.tv_title)
    TextView mTitle;
    @BindView(R.id.iv_icon)
    ImageView iv_icon;
    @BindView(R.id.shadow_toolbar)
    View shadow_toolbar;
    @BindView(R.id.app_tool_bar)
    public Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(setLayout());
        ButterKnife.bind(getActivityContext());
        onActivityCreated();
    }

    //set layout file
    public abstract int setLayout();

    //return activity context
    public abstract Activity getActivityContext();

    //set task to be done after activity creation
    public abstract void onActivityCreated();

    //set page title
    public void setPageTitle(String title, Boolean home) {
        setSupportActionBar(toolbar);
        mTitle.setText(title);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(false);
        if (home) {
            iv_icon.setVisibility(View.VISIBLE);
        } else {
            iv_icon.setVisibility(View.INVISIBLE);
        }
    }

    public void hideShadow() {
        shadow_toolbar.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                endActivity();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        endActivity();
    }

    @OnClick(R.id.iv_icon)
    public void endActivity() {
        finish();
    }

    //get String preference
    public String getPref(String key) {
        return SharedPreferencesHelper.getSharedPreferences(getActivityContext(), key, "");
    }

    //get Boolean preference
    public Boolean getBoolPref(String key) {
        return SharedPreferencesHelper.getSharedPreferences(getActivityContext(), key, false);
    }

    //get int preference
    public int getIntPref(String key) {
        return SharedPreferencesHelper.getSharedPreferences(getActivityContext(), key, 0);
    }

    //set String preference
    public void setPref(String key, String value) {
        SharedPreferencesHelper.setSharedPreferences(getActivityContext(), key, value);
    }

    //set Boolean preference
    public void setPref(String key, Boolean val) {
        SharedPreferencesHelper.setSharedPreferences(getActivityContext(), key, val);
    }

}
