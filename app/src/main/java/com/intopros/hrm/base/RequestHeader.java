package com.intopros.hrm.base;

import android.os.Build;

import com.intopros.hrm.BuildConfig;
import com.intopros.hrm.base.baseutils.sharedpreferences.SharedPreferencesHelper;
import com.intopros.hrm.base.baseutils.sharedpreferences.SharedValue;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class RequestHeader {

    public static Interceptor getHeader() {

        return new Interceptor() {
            @NotNull
            @Override
            public Response intercept(@NotNull Chain chain) throws IOException {
                Request request;
                String gcm = SharedPreferencesHelper.getSharedPreferences(AppController.getContext(), SharedValue.fcmToken, "");
                String apitoken = "apitoken";
                if (!apitoken.isEmpty())
                    apitoken = "Bearer " + apitoken;
                Request original = chain.request();
                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .addHeader("Appversion", BuildConfig.VERSION_NAME)
                        .addHeader("Androidid", Build.ID)
                        .addHeader("Gcm", gcm)
                        .addHeader("Devicename", Build.MANUFACTURER + Build.MODEL)
                        .addHeader("Authorization", apitoken);
                request = requestBuilder.build();

                return chain.proceed(request);
            }
        };
    }
}
