package com.intopros.hrm.base.baseutils.retrofit;

import com.google.gson.JsonObject;

public interface OnRes {

    void OnSuccess(JsonObject res);

    void OnError(String err, String errcode, int code, JsonObject res);
}

