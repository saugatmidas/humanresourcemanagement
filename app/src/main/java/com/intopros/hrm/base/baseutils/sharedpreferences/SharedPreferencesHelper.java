package com.intopros.hrm.base.baseutils.sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.intopros.hrm.BuildConfig;

public class SharedPreferencesHelper {

    public static String PREFS_NAME = BuildConfig.APPLICATION_ID;

    //get String preference
    public static String getSharedPreferences(Context context, String key, String defaultValue) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String prefsValues = settings.getString(key, defaultValue);
        return prefsValues;
    }

    //get Integer preference
    public static int getSharedPreferences(Context context, String key, int defaultValue) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        int prefsValues = settings.getInt(key, defaultValue);
        return prefsValues;
    }

    //get Boolean preference
    public static Boolean getSharedPreferences(Context context, String key, Boolean defaultValue) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        Boolean prefsValues = settings.getBoolean(key, defaultValue);
        return prefsValues;
    }

    //set int preference
    public static boolean setSharedPreferences(Context context, String key, int value) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        try {
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt(key, value);
            editor.apply();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    //set String preference
    public static boolean setSharedPreferences(Context context, String key, String value) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        try {
            SharedPreferences.Editor editor = settings.edit();
            editor.putString(key, value);
            editor.apply();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    //set Boolean preference
    public static boolean setSharedPreferences(Context context, String key, Boolean value) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        try {
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean(key, value);
            editor.apply();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    //clear all shared preferences
    public static void clearAll(Context context) {
        SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        settings.edit().clear().apply();
    }

}
