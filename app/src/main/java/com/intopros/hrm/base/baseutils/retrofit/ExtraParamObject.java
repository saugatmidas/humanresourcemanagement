package com.intopros.hrm.base.baseutils.retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExtraParamObject {

    @SerializedName("minversion")
    @Expose
    private Integer minversion;
    @SerializedName("forceupdate")
    @Expose
    private String forceupdate;
    @SerializedName("updatedata")
    @Expose
    private Boolean update;

    @SerializedName("nextpage")
    @Expose
    private String nextpage;

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("showdialog")
    @Expose
    private Boolean showdialog;

    public String getNextpage() {
        return nextpage;
    }

    public void setNextpage(String nextpage) {
        this.nextpage = nextpage;
    }

    public String getForceupdate() {
        return forceupdate;
    }

    public void setForceupdate(String forceupdate) {
        this.forceupdate = forceupdate;
    }

    public Boolean getUpdate() {
        return update;
    }

    public void setUpdate(Boolean update) {
        this.update = update;
    }

    public Integer getMinversion() {
        return minversion;
    }

    public void setMinversion(Integer minversion) {
        this.minversion = minversion;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getShowdialog() {
        return showdialog;
    }

    public void setShowdialog(Boolean showdialog) {
        this.showdialog = showdialog;
    }
}
