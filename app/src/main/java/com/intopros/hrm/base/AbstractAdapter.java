package com.intopros.hrm.base;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.intopros.hrm.base.baseutils.sharedpreferences.SharedPreferencesHelper;

import java.util.List;


public class AbstractAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public Activity activity;
    public List<T> mItemList;

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

    }

    public List<T> getModifyList() {
        return mItemList;
    }

    @Override
    public int getItemCount() {
        return mItemList == null ? 0 : mItemList.size();
    }

    public String getPref(Context c, String key) {
        return SharedPreferencesHelper.getSharedPreferences(c, key, "");
    }

    public Boolean getBoolPref(Context c, String key) {
        return SharedPreferencesHelper.getSharedPreferences(c, key, false);
    }

    public void setPref(Context c, String key, String value) {
        SharedPreferencesHelper.setSharedPreferences(c, key, value);
    }

    public void setBoolPref(Context c, String key, Boolean value) {
        SharedPreferencesHelper.setSharedPreferences(c, key, value);
    }
}
