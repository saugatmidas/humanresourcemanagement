package com.intopros.hrm.base.baseutils.retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseObject<T> {

    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("response")
    @Expose
    private T response;
    @SerializedName("extraparams")
    @Expose
    private ExtraParamObject extraparams;
    @SerializedName("link")
    @Expose
    private Boolean link;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getResponse() {
        return response;
    }

    public void setResponse(T response) {
        this.response = response;
    }

    public ExtraParamObject getExtraparams() {
        return extraparams;
    }

    public void setExtraparams(ExtraParamObject extraparams) {
        this.extraparams = extraparams;
    }

    public Boolean getLink() {
        return link;
    }

    public void setLink(Boolean link) {
        this.link = link;
    }

}
